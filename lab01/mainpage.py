#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file mainpage.py
@mainpage

@section sec_intro Introduction
This is the portfolio of Ashley Humpal for ME 305.

@section sec_background Background
This portfolio will contain all of the python files created Fall quarter 2020.

@section sec_fibonacci Fibonacci Calculation
This code helped to calculate the Fibonacci value at a specific index and to help me gain a better grasp of coding with Python.

Source:         https://bitbucket.org/ahumpal/me305_labs/src/master/lab01/lab01part3.py

Documentation:  https://ahumpal.bitbucket.io/lab01part3_8py.html


@section sec_elevator Elevator Tasks
This code helped to improve my understanding with Finite State Machines, and use cooperative multitasking to simulate an elevator.

Source:         https://bitbucket.org/ahumpal/me305_labs/src/master/lec%2000/elevator.py

Source for Mainpage:    https://bitbucket.org/ahumpal/me305_labs/src/master/lec%2000/elevatormain.py
                
Documentation:  https://ahumpal.bitbucket.io/elevator_8py.html

Documentation for Mainpage:    https://ahumpal.bitbucket.io/elevatormain_8py.html

@image html Elevator_FSM.png

@page page_details_page Details page
@section sec_example_section Project Information

Created on Tue Sep 29 22:11:39 2020

@author Ashley Humpal
"""

