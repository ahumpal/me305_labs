#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file lab01part3.py

Documentation for lab01part3.py, known as the Fibonnacci generator.

This package contains a Fibonnaci calculator that generates values based on the index the user inputs.
It references a non-recursive function in order to calculate the values in the Fibonnacci sequence all the way to the index value that was input by the user.
Link to source code can be found here:
    
https://bitbucket.org/ahumpal/me305_labs/src/master/lab01/lab01part3.py

@package fib Function

This file contains the Fibonnacci function package.
This package contains a Fibonnaci calculator that generates values based on the index the user inputs.


Created on Thu Sep 24, 2020

@author Ashley Humpal
"""


## Fibonacci Sequence Function
def fib (idx):

    ''' 
    @brief This method calculates a Fibonacci number corresponding to a specified index through a non-recursive function.
    @param idx An integer specifying the index of the desired
    Fibonacci number. The user enters the index number into the fib function which will calculate the Fibonnacci sequence to that index number.'''
    print ('Calculating Fibonacci number at '
           'index n = {:}.'.format(idx))
## Variable for manuscript code is created
    code=True #if the code is true do the following
    while code: #while true do this 
## Variable for input is created
        Input=input('Enter value for index or type exit:')#variable for input
       
        if Input.isdigit(): #if input is numerical do this
## Creates a list to store values
            my_list=[0, 1] #creates a list
            idx=int(Input) #indx is input
            if idx==0: #if ind is 0 do following
                print(my_list[0]) # call from 0th place of my list
               
            elif idx==1:#if inx is 1 then value will be from list
                print((my_list[1]) )  # call from 0th place of my list     
            else:
                ##Variable for a counter is created
                i=2  #beginning counter
                while i<=idx: #while the index is less than or equal to 2 do the following
## Variable for function to calculate Fibonacci values is created
                    Solved= my_list[i-1] +my_list[i-2]
                    my_list.append(Solved)
                    i +=1 #add 1 to your counter
                   # append solved value into my_list
                print (my_list)
        elif Input=='exit': #if word exit is typed do the following
            
            code=False
        else: 
            print('Please enter numerical value that is not a decimal and is positive.')
    
if __name__ =='__main__':
        fib(0)
        
    
          