#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file elevatormain.py
This file serves to implement the elevator tasks by setting known parameters to their pin equivalents,
and then running task 1 and task 2 independently.
 
Link to elevator task code can be found here:

https://bitbucket.org/ahumpal/me305_labs/src/master/lec%2000/elevatormain.py

Link to related elevator code can be found here: 
    
https://bitbucket.org/ahumpal/me305_labs/src/master/lec%2000/elevator.py

Created on Tues Oct 6, 2020

@author Ashley Humpal
"""
## Button, MotorDriver, and elevator imported from other file.
from elevator import Button, MotorDriver, elevator
#imports classes from elevator file

        
# Creating objects to pass into task constructor
## Button object called Button_1 attached to referenced pin.
Button_1 = Button('PB6')

## Button object called first attached to referenced pin.
first = Button('PB8')

## Button object called Button_2 attached to referenced pin.
Button_2 = Button('PB7')

## Button object called second attached to referenced pin.
second = Button('PB0')

## Motor object assigned to MotorDriver.
Motor = MotorDriver()

# Creating a task object using the button and motor objects above
## Task 1 created
task1 = elevator(0.1,Button_1, Button_2, first, second, Motor )

## Task 2 created
task2=elevator(0.1,Button_1, Button_2, first, second, Motor )

# Run the tasks in sequence over and over again
for N in range(10000000): # effectively while(True):
    task1.run()
    task2.run()
