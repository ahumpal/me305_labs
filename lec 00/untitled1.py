#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  4 15:29:34 2020

@author: ashleyhumpal
"""

'''
@file elevator.py

This file serves as an example implementation of a finite-state-machine using
Python. The example will implement some code to control an imaginary windshield
wiper.

The user has a button to turn on or off the windshield wipers.

There is also a limit switch at either end of travel for the wipers.
'''

from random import choice
import time

class elevator:
    '''
    @brief      A finite state machine to control windshield wipers.
    @details    This class implements a finite state machine to control the
                operation of windshield wipers.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1
    S1_MOVING_DOWN      = 1    
    
    ## Constant defining State 2
    S3_STOPPED_AT_ONE   = 3    
    
    ## Constant defining State 3
    S2_MOVING_UP        = 2    
    
    ## Constant defining State 4
    S4_STOPPED_AT_TWO   = 4
    
    ## Constant defining State 5
    S5_DO_NOTHING       = 5
    
    def floor(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([1, 2])
    
    def __init__(self, interval, Button_1, Button_2, first, second, Motor):
    #def __init__(self, interval, GoButton, LeftLimit, RightLimit, Motor):
        '''
        @brief            Creates a TaskWindshield object.
        @param button1   An object from class Button representing on/off
        @param button2  An object from class Button representing the left Limit switch
        @param first An object from class Button representing the right Limit switch
        @param second An object from class Button representing the right Limit switch
        @param Motor      An object from class MotorDriver representing a DC motor.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The button object used for the go command
        self.button1 = Button_1
        
         ## The button object used for the go command
        self.button2 = Button_2
        
        ## The button object used for the left limit
        self.first = first
        
         ## The button object used for the left limit
        self.second = second
        
        
        ## The motor object "wiping" the wipers
        self.Motor = Motor
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_MOVING_DOWN)
                self.Motor.Down()
                print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
            
            #0 to 1 here
            
            
            elif(self.state == self.S1_MOVING_DOWN):
                # Run State 1 Code
                if(self.first.getButtonState()):
                    self.transitionTo(self.S3_STOPPED_AT_ONE)
                    self.Motor.Stop
                print(str(self.runs) + ': State 1 ' + str(self.curr_time-self.start_time))
            
            #1 to 3 
            
            
            
            
            elif(self.state == self.S3_STOPPED_AT_ONE):
                # Run State 2 Code
                 if(self.runs*self.interval > 10):  # if our tsk runs for 10 seconds, transition
                    self.transitionTo(self.S5_DO_NOTHING)
                 if(self.Button_2.getButtonState()):
                    self.transitionTo(self.S2_MOVING_UP)
                    self.Motor.Up()
                    print(str(self.runs) + ': State 2 ' + str(self.curr_time-self.start_time))
            # 3 to 2 
            
            
            
            
            elif(self.state == self.S2_MOVING_UP):
                # Transition to state 1 if the left limit switch is active
                if(self.second.getButtonState()):
                    self.transitionTo(self.S4_STOPPED_AT_TWO)
                    self.Motor.Stop()
                print(str(self.runs) + ': State 3 ' + str(self.curr_time-self.start_time))
                #2 to 4 
                
    
            
            elif(self.state == self.S4_STOPPED_AT_TWO):
                # Run State 4 Code
                if(self.Button_1.getButtonState()):
                    self.transitionTo(self.S1_MOVING_DOWN)
                    self.Motor.Down()
                print(str(self.runs) + ': State 4 ' + str(self.curr_time-self.start_time))
                #4 to 1 
                
                
            elif(self.state == self.S5_DO_NOTHING):
                # Run state 5 code
                print(str(self.runs) + ': State 5 ' + str(self.curr_time-self.start_time))
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        

class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary driver to turn on or off the wipers. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([True, False])

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the wipers
                wipe back and forth.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Up(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor moving CCW')
    
    def Down(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor moving CW')
    
    def Stop(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor Stopped')



























