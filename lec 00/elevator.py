#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file elevator.py

This file serves as an implementation of a finite-state-machine using
Python. It will implement some code to control an imaginary elevator.

The user has 2 buttons; Button 1 for telling the elevator to go to floor 1, and Button 2 for telling the elevator to go to floor 2.

Link to source code can be found here: 
    
https://bitbucket.org/ahumpal/me305_labs/src/master/lec%2000/elevator.py

Link to related elevator mainpage code can be found here: 
    
https://bitbucket.org/ahumpal/me305_labs/src/master/lec%2000/elevatormain.py

Created on Tues Oct 6, 2020

@author Ashley Humpal
"""

from random import choice
import time

## New class called elevator
class elevator:
    '''
    @brief      A finite state machine to control windshield wipers.
    @details    This class implements a finite state machine to control the
                operation of windshield wipers.
    '''
    
## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
## Constant defining State 1
    S1_MOVING_DOWN      = 1    
    
## Constant defining State 2
    S2_MOVING_UP        = 2 
    
## Constant defining State 3  
    S3_STOPPED_AT_ONE   = 3    
    
## Constant defining State 4
    S4_STOPPED_AT_TWO   = 4
    
## Constant defining State 5
    S5_DO_NOTHING       = 5
    
    #def getfloor_1(self):
      
    #def getfloor_2(self):
      
    def __init__(self, interval, Button_1, Button_2, first, second, Motor):
    #def __init__(self, interval, GoButton, LeftLimit, RightLimit, Motor):
        '''
        @brief          Creates an elevator object.
        @param button1  An object from class Button representing Button 1 created.
        @param button2  An object from class Button representing Button 2 created.
        @param first    An object from class Button representing the first floor.
        @param second   An object from class Button representing the second floor.
        @param Motor    An object from class MotorDriver representing a DC motor.
        '''
        
## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
## The button object used for Button 1
        self.Button_1 = Button_1
        
## The button object used for Button 2
        self.Button_2 = Button_2
        
## The button object used for the first floor
        self.first = first
        
## The button object used for the second floor 
        self.second = second
        
        
## The motor object driving the elevator
        self.Motor = Motor
        
## Counter that describes the number of times the task has run
        self.runs = 0
        
##  The amount of time in seconds between runs of the task
        self.interval = interval
        
## The timestamp for the first iteration
        self.start_time = time.time()
        
## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time() #initializes variable for time
        if self.curr_time > self.next_time: #if the current time exceeds time of when next task runs do following
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_MOVING_DOWN) #begins transition
                self.Motor.Down(2) #tells motor to move down
                print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
                print('Elevator Moving Down')
            #0 to 1 here
            
            
            elif(self.state == self.S1_MOVING_DOWN):
                # Run State 1 Code
                if(self.first.getButtonState(1)):# elevator at first floor
                    self.transitionTo(self.S3_STOPPED_AT_ONE) #transition to state 3
                    self.Motor.Stop(0) #stop motor
                print(str(self.runs) + ': State 1 ' + str(self.curr_time-self.start_time))
                print('Elevator Stopped at Floor 1')
            #1 to 3 
            
            
            
            
            elif(self.state == self.S3_STOPPED_AT_ONE):
                # Run State 3 Code
                 if(self.runs*self.interval > 1):  # if our task runs longer then interval, transition
                    self.transitionTo(self.S5_DO_NOTHING)
                    
                 if(self.Button_2.getButtonState(1)): #elevator button 2 pressed
                    self.transitionTo(self.S2_MOVING_UP) #transition to moving up
                    
                    self.Button_2.getButtonState(0) #elevator button 2 no longer pressed
                    self.first.getButtonState(0) # elevator button 1 also not presses (clearing)
                    
                    self.Motor.Up(1) #tell motor to move up 
                    print(str(self.runs) + ': State 3 ' + str(self.curr_time-self.start_time))
                    print('Elevator Moving Up')
            # 3 to 2 
            
            
            
            
            elif(self.state == self.S2_MOVING_UP):
                # Run State 2 Code
                if(self.second.getButtonState(1)): #elevator stopping at second floor
                   
                    self.transitionTo(self.S4_STOPPED_AT_TWO) #cues transition to stop
                    self.Motor.Stop(0) #motor stop
                print(str(self.runs) + ': State 2 ' + str(self.curr_time-self.start_time))
                print('Elevator Stopping at Floor 2')
                #2 to 4 
                
    
            
            elif(self.state == self.S4_STOPPED_AT_TWO):
                # Run State 4 Code
                if(self.Button_1.getButtonState(1)): #button 1 pressed
                    self.transitionTo(self.S1_MOVING_DOWN) #cue transition
                    
                    self.second.getButtonState(0) #no longer at second floor
                    self.Button_1.getButtonState(0) #clear button 1
                    self.Motor.Down(2) #motor goes down
                print(str(self.runs) + ': State 4 ' + str(self.curr_time-self.start_time))
                #4 to 1 
                print('Elevator Moving Down')
                
                
            elif(self.state == self.S5_DO_NOTHING):
                # Run state 5 code
                print(str(self.runs) + ': State 5 ' + str(self.curr_time-self.start_time))
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState): #defines transitionTo state
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
    
        
        

class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary driver to turn on or off the wipers. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self,va):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        if va == 1: #if va equals this number then do following
            return choice ([True, False])
        else:
            return False
       
    
class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the wipers
                wipe back and forth.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Up(self,ve):
        '''
        @brief Moves the motor up
        '''
        print('Motor moving Up to floor 2')
        if ve == 1:#if ve equals this number then do following
            return choice ([True])
            
        else:
            return False
        
    
    def Down(self,vd):
        '''
        @brief Moves the motor down
        '''
        print('Motor moving Down to floor 1')
        if vd == 2:#if vd equals this number then do following
            return choice ([True])
            
        else:
            return False
        
    
    def Stop(self,vf):
        '''
        @brief Stops movement of motor
        '''
        print('Motor Stopped')
        if vf == 0:#if vf equals this number then do following
            return choice ([True])
            
        else:
            return False
        



























