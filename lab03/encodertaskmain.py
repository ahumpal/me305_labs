#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file encodertaskmain.py
This file serves to implement the Encoder FSM and the User Interface by setting known parameters to their hardware equivalents,
and then running task 1 and task 2 independently.
 
Link to source code can be found here:

https://bitbucket.org/ahumpal/me305_labs/src/master/lab03/encodertaskmain.py

Created on Tues Oct 6, 2020

@author Ashley Humpal
"""
## encoder, EncoderFSM, and UI imported from other file.
from Encoderclass import encoder
from EncoderFSM import EncoderFSM
from UI import UI
#imports classes from other files 


## Task 1 created
task1 = encoder(0.1)

## Task 2 created
task2=UI(0.1)

# Run the tasks in sequence over and over again
for N in range(10000000): # effectively while(True):
    task1.run()
    task2.run()

