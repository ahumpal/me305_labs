#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file EncoderFSM.py

This file serves as an implementation of a finite-state-machine using
Python. It will implement some code to control the encoder.

Link to source code can be found here: 
    
https://bitbucket.org/ahumpal/me305_labs/src/master/lab03/EncoderFSM.py

Created on Tues Oct 13, 2020

@author Ashley Humpal
"""

from random import choice
from Encoderclass import encoder
import utime 

## New class called EncoderFSM
class EncoderFSM:
    '''
    @brief      A finite state machine to control windshield wipers.
    @details    This class implements a finite state machine to control the
                operation of windshield wipers.
    '''
    
## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
## Constant defining State 1
    S1_UPDATING     = 1    
    
      
    def __init__(self, encoder, interval):
    #def __init__(self, interval, GoButton, LeftLimit, RightLimit, Motor):
        '''
        @brief          Creates an encoder object.
        
        '''
        
## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        

## Counter that describes the number of times the task has run
        self.runs = 0
        
##  The amount of time in seconds between runs of the task
        #self.interval = interval
        self.interval= int(interval*1e3)
        
## The timestamp for the first iteration
        #PYTHON: self.start_time = time.time()
        self.start_time = utime.ticks_us()
        
## The "timestamp" for when the task should run next
       # self.next_time = self.start_time + self.interval
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        #self.curr_time = time.time() #initializes variable for time
        self.curr_time = utime.ticks_us()
        #if self.curr_time > self.next_time: #if the current time exceeds time of when next task runs do following
        if (utime.ticks_diff(self.curr_time, self.next_time)>=0):
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_UPDATING) #begins transition
                #print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
                #print('MOVING TO GETTING UPDATE')
            #0 to 1 here
            
            
            elif(self.state == self.S1_UPDATING): #State 1
                # Run State 1 Code
                self.encoder.update()
                #print(str(self.runs) + ': State 1 ' + str(self.curr_time-self.start_time))
               # print('UPDATED VALUE')
               # print (self.position)
            #1 to 1
            
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
           # self.next_time = self.next_time + self.interval
            self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    def transitionTo(self, newState): #defines transitionTo state
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
    
 


























