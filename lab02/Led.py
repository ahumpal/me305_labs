#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file Led.py

This file serves as an implementation of a finite-state-machine using
Python. It will implement code to control a LED and its blinking light.

This script has code for the classes necessary to either turn the virtual LED on or off, or change the physical LED brightness in accordance to the pattern of a saw-tooth wave form. 
Link to source code can be found here: 
    
https://bitbucket.org/ahumpal/me305_labs/src/master/lab02/Led.py

Link to related LED mainpage code can be found here: 
    
https://bitbucket.org/ahumpal/me305_labs/src/master/lab02/Ledmain.py

Created on Fri Oct 9, 2020

@author Ashley Humpal
"""

from random import choice
import time
# MICROPYTHON: import pyb
# MICROPYTHON: import utime 

## New class called light
class light:
    '''
    @brief      A finite state machine to control the blinking of a LED.
    @details    This class implements a finite state machine to control the
                operation of LEDs.
    '''
    
## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
## Constant defining State 1 - LED ON
    S1_ON               = 1    
    
## Constant defining State 2 - LED OFF
    S2_OFF              = 2 
    

      
    def __init__(self, interval, existence):
   
        '''
        @brief          Creates a light object.
        @param ON  An object from class light representing ON is created.
        @param OFF An object from class light representing OFF is created.
        '''
        
## The state to run on the next iteration of the task.
        self.state = self.S0_INIT

## The button object used for checking whether what's being run is for the virtual or real LED
        self.existence = existence
        
## Counter that describes the number of times the task has run
        self.runs = 0
        
##  The amount of time in seconds between runs of the task
        self.interval = interval
        #MICROPYTHON: self.interval= int(interval*1e3)
        
## The timestamp for the first iteration
        self.start_time = time.time()
        # MICROPYTHON: self.start_time = utime.ticks_us()
        
## The "timestamp" for when the task should run next
        self.next_time = self.start_time + (1/11)*self.interval #1/11 because 11 iterations to complete
        # MICROPYTHON: self.next_time = utime.ticks_add(self.start_time, (91)*self.interval)
        
## The iterative counter used for tasks 
        self.iteration = 0
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        
       
        #MICROPYTHON:pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
        #MICROPYTHON:tim2 = pyb.Timer(2, freq = 20000)
        #MICROPYTHON:t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
        
        
        self.curr_time = time.time() #initializes variable for time
        # MICROPYTHON: self.curr_time = utime.ticks_us()
        
        if self.curr_time > self.next_time: #if the current time exceeds time of when next task runs do following
        # MICROPYTHON: if (utime.ticks_diff(self.curr_time, self.next_time)>=0):
            if(self.state == self.S0_INIT): # do following if at specified state
    
                self.transitionTo(self.S1_ON) #begins transition to next state
            #0 to 1 here
            
            
            elif(self.state == self.S1_ON): #if at state 1 do following
                # Run State 1 Code
                if(self.existence==0): #if existence is 0 meaning virtual LED, do following
                   self.transitionTo(self.S2_OFF) #transition to state 3
                   self.iteration=0
                   print('LED OFF')
                   
                   
                elif(self.existence==1): # if existence is real LED do following
                     self.iteration = self.iteration +10 #counts for 10 iterations
                     print(self.iteration)
                     #MICROPYTHON: t2ch1.pulse_width_percent(self.iteration)
                     if self.iteration>90:
                      self.transitionTo(self.S2_OFF) #transition to state 2
            #1 to 2
            
            elif(self.state == self.S2_OFF): #if at this state do following
                # Run State 1 Code
         
                if(self.existence==0):
                   self.transitionTo(self.S1_ON) #transition to state 1
                   self.iteration=100
                   print('LED ON')
                   
                elif(self.existence==1):
                    self.iteration=0
                    print(self.iteration)
                    #MICROPYTHON: t2ch1.pulse_width_percent(self.iteration)
                    self.transitionTo(self.S1_ON) #transition to state 1
             
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
           
            self.next_time += (1/11)*self.interval
            # MICROPYTHON: self.next_time = utime.ticks_add(self.next_time, (91)*self.interval)
            #91 conversion of 1/11 with milli-seconds
    
    def transitionTo(self, newState): #defines transitionTo state
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
   
        
 
  


























