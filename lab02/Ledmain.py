#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file Ledmain.py
This file serves to implement the Fine State Machine by setting known parameters to their pin equivalents,
and then running task 1 and task 2 independently. Task 1 is running the virtual LED, while Task 2 is running the real LED pattern.
 
Link to LED task code can be found here:

https://bitbucket.org/ahumpal/me305_labs/src/master/lab02/Ledmain.py

Link to related LED code can be found here: 
    
https://bitbucket.org/ahumpal/me305_labs/src/master/lab02/Led.py

Created on Fri Oct 9, 2020

@author Ashley Humpal
"""
## light imported from other file.
from Led import light
#imports classes from elevator file



# Creating a task object using the light above
## Task 1 created
task1 = light(10,1)

## Task 2 created
task2=light(10,0)

# Run the tasks in sequence over and over again
for N in range(10000000000): # effectively while(True):
    task1.run()
    task2.run()
