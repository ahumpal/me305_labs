#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file UIfrontend.py

This file serves as a user interface, which can be paired with the Data Collection File.
It can send and read user inputs and outputs from other files. It plots all received data and creates an excel file. 

Link to source code can be found here: 
    
https://bitbucket.org/ahumpal/me305_labs/src/master/lab04/UIfrontend.py

Created on Thu Oct 22 13:01:03 2020

@author: ashleyhumpal
"""

import serial
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import keyboard 

## Variable representing serial connection
ser = serial.Serial(port='/dev/tty.usbmodem14203',baudrate=115273,timeout=1)
## Empty list representing position
x=[]
## Empty list representing time
t=[]
    
#Function to write inputs
def sendChar():
## Variable representing user input
    inv = input('Give me a character. Type "G" to have Nucleo begin sampling from the encoder. Type "S" to stop data collection at anytime. ')
    ser.write(str(inv).encode('ascii')) #sends it to the board
 
#Function to read outputs from other files
def receiveChar():
    while True:
        myval = ser.readline().decode('ascii')
        if myval !=0:  #if any key is read
            line_list = myval.strip().split(',')	# strip any special characters and then split the string into wherever a comma appears;
            t.append(int(line_list[0])) #append values to list t
            x.append(int(line_list[1])) #append values to list x

            
            if keyboard.is_pressed('S'):
                break #stop whats happening
                
                
            elif len(x)>50: 
                break #stop whats happening
           
      
        else :
            pass
      
  
  
    plt.plot(t, x)
    plt.xlabel('Time [s]')
    plt.ylabel('Position [Degrees]')
    data = list(zip(t,x))
    
    np.savetxt('AshleyHumpal_array.csv',data, delimiter =',')  
   

if __name__ == '__main__': 
    sendChar()
    receiveChar()  
  
  
  
ser.close()  
  
  
