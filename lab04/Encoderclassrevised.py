#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file Encoderclassrevised.py

This file serves as class file for the encoder driver class in
Python. It will implement functions and commands using the encoder.

This script has code for the encoder class that utilizes the encoder's timer. 
Link to source code can be found here: 
    
https://bitbucket.org/ahumpal/me305_labs/src/master/lab04/Encoderclassrevised.py

Created on Fri Oct 13, 2020

@author Ashley Humpal
"""

#LEFT IN MICROPYTHON INSTEAD OF PYTHON BECAUSE ALL CODE DEPENDS ON MICRO 
import pyb

## New class called encoder
class encoder:
    '''
    @brief      A finite state machine to control the 
    @details    This class implements 
    '''


      
    def __init__(self):
   
        '''
        @brief          Creates a object.
        @param pinA6  An object from class encoder representing pinA6.
        @param pinA7 An object from class encoder representing pinA7.
        @param tim An object rom class encoder representing the encoder counter.
        '''
        

## The encoder object for the final position is created
        self.position= 0
        
## The encoder object for the previus position is created
        self.previousposition=0
        
## The encoder object for the period length is created
        self.period=0xFFFF
        
## The encoder object for the timer is created      
        self.tim= pyb.Timer(3) #Creating variable for the encoder timer
## The encoder object for the prescaler is created.
        self.tim.init(prescaler=0, period=self.period) # initializes timer parameters
## The encoder object for the pins are created.
        self.tim.channel(1, pin=pyb.Pin.cpu.A6, mode=pyb.Timer.ENC_AB) #sets up channel 1
## The encoder object for the encoder mode is created.

## The encoder object for A7 is created.
        self.tim.channel(2, pin=pyb.Pin.cpu.A7, mode=pyb.Timer.ENC_AB) #sets up channel 2
   
        
    def update(self): 
        '''
        @brief      Updates the position of the encoder by adding good delta values
        '''
## The encoder object for the current position is created
        self.currentposition= self.tim.counter() #current position
        self.delta=self.currentposition-self.previousposition
        self.previousposition=self.currentposition
## The encoder object for the delta value between runs is created.
        self.deltamag = abs(self.delta)
## The encoder object for the absolute value of delta is created
        #print(self.tim.period)
       
        if self.deltamag> .5*(self.period): 
            if self.delta<0:
               self.updateddelta = self.delta +self.period
                              
            elif self.delta>0:
                self.updateddelta = self.delta - self.period
                
        else:
            self.updateddelta = self.delta

        self.position= self.position+ self.updateddelta
        
    def getposition(self):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        # return self.tim.counter()
        return self.position
    
    def setposition(self,datum):
        '''
        @brief      Updates the variable defining the next state to run
        '''        
        return self.tim.counter(datum)
    
        #self.tim.callback(None)
    def getdelta(self):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        
        return self.updateddelta
        
    
    
