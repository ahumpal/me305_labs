#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file DataCollection.py

This file serves as an implementation of a finite-state-machine using
micropython. It will implement code to start and stop sampling data from the encoder when it reads certain key inputs.

Link to source code can be found here: 
    
https://bitbucket.org/ahumpal/me305_labs/src/master/lab04/DataCollection.py

Created on Sun Oct 25 16:58:04 2020

@author: ashleyhumpal
"""
from Encoderclassrevised import encoder
import utime 
from pyb import UART
## Variable representing nucleo board connection
myuart=UART(2)
## Variable representing myuart
ser=myuart
#importing neccessary information


## New class called collecting
class collecting: 
    '''
    @brief      A finite state machine to control the data collection of the encoder.
    @details    This class implements a finite state machine to control the
                sampling of time and position data points from the encoder.
    '''
    
## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
## Constant defining State 1
    S1_WAITING_USER     = 1    
    
## Constant defining State 2
    S2_COLLECTING     = 2 


  
    
      
    def __init__(self, encoder, interval):
        '''
        @brief          Creates an encoder object.
        
        '''
        
## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
## Intializes the encoder.
        
        self.encoder=encoder
        

## Counter that describes the number of times the task has run
        self.runs = 0
        
##  The amount of time in milliseconds between runs of the task
        self.interval= int(interval*1e3)
        
## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

## An empty list representing position
        self.x=[]

## An empty list representing time       
        self.t=[]
        
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
## Variable representing current time
        self.curr_time = utime.ticks_ms() #sets current time
        if (utime.ticks_diff(self.curr_time, self.next_time)>=0): #condition for state
            if(self.state == self.S0_INIT): 
                self.transitionTo(self.S1_WAITING_USER) #begins transition
            #0 to 1 here
            
            
            elif(self.state == self.S1_WAITING_USER): #if at state 1 do following 
                self.encoder.update() #update encoder position
                #print('Please select G or S to begin')
                if myuart.any() !=0: #if keyy pressed 
                    crt= ser.readchar() #variable to read key 
                    if crt==71:# if key is g then do following
                        self.encoder.setposition(0) #set position to 0
                        self.transitionTo(self.S2_COLLECTING) #state transition
                    elif crt==83:#if s key then:
                     self.transitionTo(self.S1_WAITING_USER) #transition back to state 1
                    else :
                        #print('Please only type "G" to start data collection or "S" to stop data collection')
                        self.transitionTo(self.S1_WAITING_USER)   #any other key then transition back to state 1
                        
                        
            elif(self.state == self.S2_COLLECTING): #if state 2 then do following
                       self.encoder.update()#update position
                       self.encoder.getposition()#get position value
                       ser.write('{:},{:}'.format (self.t, self.x))
                       if (self.interval*self.runs>=10000): #if 10 seconds have passed do following
                               self.t.clear()#clear time list
                               self.runs=-1;#reset counter
                               #print('Time interval reached')
                               self.transitionTo(self.S1_WAITING_USER)
                              # self.x.clear()
                               #self.transitionTo(self.S3_NOTHING) #transition to state 3
                       elif ser.readchar() ==83:#s
                            
                            self.t.clear()
                            self.runs=-1;
                            self.transitionTo(self.S1_WAITING_USER)
                            #self.x.clear()
                            #self.transitionTo(self. S3_NOTHING )
                       else :
                           self.transitionTo(self. S2_COLLECTING )

                       self.runs += 1
                
            
                       
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState 
         
 