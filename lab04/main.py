#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file main.py
This file serves to run the Data Collection File by importing all the necessary classes.

Link to source code can be found here: 
    
https://bitbucket.org/ahumpal/me305_labs/src/master/lab04/main.py

Created on Thu Oct 22 13:07:21 2020

@author: ashleyhumpal
"""
from pyb import UART
#myuart = UART(2)
from Encoderclassrevised import encoder
from DataCollection import collecting


myencoder = encoder()

## Task 1 created

task1 = collecting(myencoder, .2)

    

while True: #cuz run for 10 secs total
    task1.run()
    
    
 