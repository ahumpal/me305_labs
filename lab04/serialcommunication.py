#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 13:01:03 2020

@author: ashleyhumpal
"""

import serial

ser = serial.Serial(port='/dev/tty.usbmodem14203',baudrate=115273,timeout=1)

def sendChar():
    inv = input('Give me a character: ')
    ser.write(str(inv).encode('ascii'))
    myval = ser.readline().decode('ascii')
    return myval

for n in range(1):
    print(sendChar())

ser.close()
                                  