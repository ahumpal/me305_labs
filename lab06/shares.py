#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
@file shares.py
@brief A container for all the inter-task variables
@author Ashley Humpal

'''

## The command character sent from the user interface task to the backend and then the controller task
Kp    = None

## Omega reference values sent throught the backend
omegaref   = None

## Omega actual values recorded in controller task and sent to the frontend through the backend
omegaact    = None
## Time values recorded in controller task and sent to the frontend through the backend
times= None