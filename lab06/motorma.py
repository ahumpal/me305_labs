#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file motorma.py
Created on Sun Nov 22 15:02:56 2020

This file runs the Closed Loop Controller task and the Backend.


@author Ashley Humpal
"""
import pyb
from Encoderclassnew import encoder
from Closed_loop import ClosedLoop
from Motor_Driver import MotorDriver
from Closed_loop_task import ControllerFSM
from CLbackend import Backend

## Motor Driver object created
moe1= MotorDriver( pyb.Pin.cpu.A15, pyb.Pin.cpu.B4, 1, pyb.Pin.cpu.B5, 2, 3) 

## Closed Loop object created    
CL= ClosedLoop()

## Encoder object created 
myencoder= encoder()


## Task 1 created using the CL controller FSM
task1 = ControllerFSM( moe1, CL, myencoder, .02)
## Task 2 created using the Backend
task2= Backend(.01)



# Run the tasks in sequence over and over again
while True:
    task1.run()
    task2.run()
   

