#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file CLfrontend.py

Created on Fri Nov 20 19:01:52 2020


This file serves as a user interface, which is paired with the CL backend and Closed Loop Task.
It prompts the user for a Kp value and then runs the motor for 5 seconds. It then plots the step response. 

Please that a Kp value above 0.3 causes overshoot. Please also note that I was only able to complete one run at time. If you want to use a another Kp value you have to rerun the frontend and main.


Created on Thu Oct 22 13:01:03 2020

@author Ashley Humpal
"""

import serial
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import time

## Variable representing serial connection
ser = serial.Serial(port='/dev/tty.usbmodem14103',baudrate=115273,timeout=1)

## Formatting list
Lis=[]


## List representing the actual omega values
omegaact1=[]

## Empty list representing time
times1=[]
# List Kp is appended to
la=[]

#Function to write inputs
def sendChar():
## Variable representing user input
    Kp = float(input("Enter numerical Kp value:")) #Prompts user for input 
    la.append(Kp) #appends users input into list la 
    ser.write('{:f}\r\n'.format(la[0]).encode('ascii')) #writes list 
    
 
#Function to read outputs from other files
def receiveChar():
    time.sleep(4.96) #delay needed to give motor time to produce values before reading 
    # Note that there is a rough FSM at the end of this file. I was not able to get it to work hence this delay.
## Variable representing the decode line
    myval = ser.readline().decode('ascii') #decode information from the backedn
    if myval !=0:  #if any key is read
## Intermediate variable stripping and splitting 
        line_list = myval.strip('[]\r\n').split('];[')	# strip any special characters and then split the string
        print(line_list[0])
        print(line_list[1])
        times=(line_list[0].split(',')) #time values moved to line_list position 0 and split 
        omegaact=(line_list[1].split(',')) #omega actual values moved to line_list position 1 and split 
        for n in range (len(omegaact)): #for the length of the omega actual list do the following:
            omegaact1.append(float(omegaact[n])) #append values into new list called omegaact1
            times1.append(float(times[n])/1000) #append values into new list called times1

    plt.plot(times1, omegaact1) # plot these arrays
    step=[900]*250 #creates step array
    plt.plot(times1, step) #plot these arrays
    plt.xlabel('Time [s]') #x axis label
    plt.ylabel('Velocity [RPM]') #y axis label
    plt.legend(['Omega Actual','Omega Desired']) #legend
    plt.title('Step Response for 900 RPM') #title
    
if __name__ == '__main__': 
        sendChar()
        receiveChar()
   #below is how I would run multiple runs 
    
    # for n in range(3): 
    #     sendChar()
    #     receiveChar()
    #     n=+1
    
    
ser.close()

#below this is a FSM I was trying to get working!!!! 


# ## New class called FSM
# class ControllFront:
#     '''
#     @brief      A finite state machine to control windshield wipers.
#     @details    This class implements a finite state machine to control the
#                 operation of windshield wipers.
#     '''
    
# ## Constant defining State 0 - Initialization
#     S0_INIT             = 0    
    
# ## Constant defining State 1
#     S1_INPUTS     = 1    
    
# ## Constant defining State 1
#     S2_BACKTOFRONT   = 2    
    
# ## Constant defining State 1
#     S3_PLOTTING    = 2    
    
    
 
    
      
#     def __init__(self,  interval, omegaact):
#         '''
#         @brief          Creates an encoder object.
        
#         '''
        
# ## The state to run on the next iteration of the task.
#         self.state = self.S0_INIT
        
# ## ## Counter that describes the number of times the task has run
#         self.runs = 0
        
# ##  The amount of time in seconds between runs of the task
#         self.interval = interval
        
# ## The timestamp for the first iteration
#         self.start_time = time.time()
        
# ## The "timestamp" for when the task should run next
#         self.next_time = self.start_time + self.interval
        
#         self.Lis=[]
        
        
#     def run(self):
#         '''
#         @brief      Runs one iteration of the task
#         '''
#         self.curr_time = time.time() #initializes variable for time
#         if self.curr_time > self.next_time: #if the current time exceeds time of when next task runs do following
            
#             if(self.state == self.S0_INIT):
#                 self.transitionTo(self.S1_INPUTS)
                
            
#             elif(self.state == self.S1_INPUTS):
#                 # Run State 1 Code
#                 self.Kp = float(input("Enter Kp value"))
#                 self.omegaref = [800]*500
#                 self.Lis.append(self.Kp) 
#                 self.Lis.append(self.omegaref)
#                 self.ser.write('{:f};{:f}\r\n'.format(self.Lis[0], self.Lis[1]).encode('ascii'))
#                 self.transitionTo(self.S2_BACKTOFRONT)
#             #0 to 1 
            
            
            
#             elif(self.state == self.S2_BACKTOFRONT):
#                 while True:
#                     myval = ser.readline().decode('ascii')
#                     if myval !=0:  #if any key is read
#                         line_list = myval.strip().split(';')	# strip any special characters and then split the string into wherever a comma appears;
#                         times=(int(line_list[0])) #append values to list t
#                         omegaact=(int(line_list[1])) #append values to list x
#                         self.transitionTo(self.S3_PLOTTING)
                        
                
                        
#             elif(self.state == self.S3_PLOTTING):
#                 plt.plot(times, omegaact)
#                 plt.xlabel('Time [s]')
#                 plt.ylabel('Position [Degrees]')
#                 self.transitionTo(self.S1_INPUTS)
                
               
#             else:
#                 # Invalid state code (error handling)
#                 pass
            
#             self.runs += 1
            
#             # Specifying the next time the task will run
#             self.next_time = self.next_time + self.interval
    
#     def transitionTo(self, newState): #defines transitionTo state
#         '''
#         @brief      Updates the variable defining the next state to run
#         '''
#         self.state = newState
        
 
#     if __name__ == '__main__':
      
 