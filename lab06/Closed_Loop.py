#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file Closed_Loop.py

This file serves as class file for the Closed Loop to calculate the effort of the motor.


Created on Thu Nov 12 14:09:17 2020

@author Ashley Humpal
"""

import pyb
from pyb import UART



class ClosedLoop:
    '''
    @brief      A class that utilizes the controller gain value.
    
    '''
    
    
    def __init__(self):

        '''
        @brief              Creates a TaskWindshield object.
        @param nSLEEP_pin   A pyb.Pin object to use as the enable pin to power the board.
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
        @param tim3         A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin. 
        '''  
## Proportional Controller gain initialized      
        self.Kp=0
## Vm initialized
        self.vm=3.3
## Motor effort initialized 
        self.L=0
## Omega actual initialized 
        self.omegaact=0
## Omega reference initialized 
        self.omegaref=0

    def get_Kp(self):
        '''
        @brief      Returns the controller gain value
        '''
        return self.Kp
    
    def set_Kp(self,val):
        '''
        @brief      Sets controller gain value 
        '''        
        self.Kp=val
           
    def update (self):
        '''
        @brief      This method calculates the effort needed of the motor to reach desired omega. 
        
        '''
        self.kpprime= self.Kp/self.vm
        self.L= self.kpprime*(self.omegaref-self.omegaact)

        if self.L>=100:
            self.L=100
        elif self.L<=-100:
            self.L=-100
        else:
            pass
       

       
# if __name__ == '__main__':
#     mycl= ClosedLoop()
#     mycl.Kp=1
#     mycl.omegaref=1
#     mycl.omegaact=.8
#     mycl.update()
#     print(mycl.L)
        
       
      