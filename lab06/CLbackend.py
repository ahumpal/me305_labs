#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file CLbackend.py

This file serves as the backend. It helps the frontend and CL task communicate with eachother and send data to each other.
It can send and read user inputs and outputs from other files through its FSM. 
Note that the omega desired array is  nested within the backend so that needs to be changed manually.  


Created on Sun Nov 22 12:01:03 2020

@author Ashley Humpal
"""


import utime 
from pyb import UART
## Variable representing nucleo board connection
myuart=UART(2)
from Motor_Driver import MotorDriver
import shares

## New class called Backend
class Backend:
    '''
    @brief      A finite state machine to facilitate communication between the frontend and controller task.
    @details    This class implements a finite state machine to send Kp to the Controller task, and then send the time and omega actual values to the frontend afterwards. 
               
    '''
    
## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
## Constant defining State 1
    S1_READFROMFRONTINBACK    = 1             
        
## Constant defining State 2
    S2_WRITEFROMFRONTOTASK     = 2    
    
## Constant defining State 3
    S3_READINGFROMTASK     = 3             
    
## Constant defining State 4
    S4_WRITINGFROMTASKTOFRONT     = 4            
     
    def __init__(self, interval):
        '''
        @brief          Creates Backend object.
        
        '''
        
## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
## Counter that describes the number of times the task has run
        self.runs = 0
        
##  The amount of time in milliseconds between runs of the task
        self.interval= int(interval*1e3)
        
## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
 
## List to represent time
        self.times=[]
## List to represent omega actual values
        self.omegaact=[]
## List to store time values
        self.Lis1=[]
## List to store omega actual values
        self.Lis2=[]
## Intermediate list 
        self.li=[]
## Setting up shares omega reference values
        shares.omegaref=[800]*251 #changed this from 251 which was for 5 secs, but now for 4.96
## An independent counter
        self.count=0
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
## Variable representing current time
        self.curr_time = utime.ticks_ms() #sets current time
        if (utime.ticks_diff(self.curr_time, self.next_time)>=0): #condition for state
            if(self.state == self.S0_INIT): 
                self.transitionTo(self.S1_READFROMFRONTINBACK) #begins transition
            #0 to 1 here
            
            elif(self.state == self.S1_READFROMFRONTINBACK): #if at state 1 do following 
                if myuart.any() != 0: #if key pressed
                    self.lstr = myuart.readline().decode('ascii') #decode string
                    self.li = self.lstr.strip('\r\n').split(';') #strip and split
                    self.Kp = (self.li[0])  #store Kp to list
                    shares.Kp = float(self.Kp) #store Kp as float in shares      
                    #shares.omegaref=[800]*251 #shares omega reference restated
                    self.omegaref = [800]*251 #set shares omega ref to self omegaref
                    self.count=0 #reset counter to 0
                    self.transitionTo(self.S2_WRITEFROMFRONTOTASK)     #transition      


            elif(self.state == self.S2_WRITEFROMFRONTOTASK):#if state 2
                    shares.omegaref=self.omegaref[self.count] #set share omegaref to corresponding value from list
                    self.count+=1# add to count
                    self.transitionTo(self.S3_READINGFROMTASK) #transition to 3 
                    if self.runs>=250 :#if runs over 250 (5 seconds)
                        self.transitionTo(self.S4_WRITINGFROMTASKTOFRONT) #transition
                    else:
                        pass 
                    self.runs += 1   #add to runs
                   
            elif(self.state == self.S3_READINGFROMTASK): #if state 3
                    if shares.omegaact != None: # if theres an omega actual value
                        self.Lis1.append(shares.times) #append times to list 1
                        self.Lis2.append(shares.omegaact) #append omega act to list 2
                        shares.times= None #clear variable
                        shares.omega= None #clear variable
                        self.transitionTo(self.S2_WRITEFROMFRONTOTASK) #transition to state 2
                
            elif(self.state == self.S4_WRITINGFROMTASKTOFRONT): #if state 4
                myuart.write('{:};{:}\r\n'.format(self.Lis1, self.Lis2).encode('ascii')) #write list
                #print(self.Lis1)
                #print(self.Lis2)
                self.Lis1=[]#reset list
                self.Lis2=[]#reset list
                self.transitionTo(self.S1_READFROMFRONTINBACK) #transition state 1

                
            self.next_time = utime.ticks_add(self.next_time, self.interval)             
                       
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState 
        
 
  