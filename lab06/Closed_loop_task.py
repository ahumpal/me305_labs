#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file Closed_loop_task.py

This file serves as the Closed Loop controller. It runs the motor for 5 seconds while adjusting the motor effort so that the omega actual is as close to omega reference as possible. 


Created on Fri Nov 20 18:56:05 2020

@author Ashley Humpal
"""

from Encoderclassnew import encoder
from Closed_loop import ClosedLoop
from Motor_Driver import MotorDriver
import utime 
from pyb import UART
## Variable representing nucleo board connection
myuart=UART(2)

#importing neccessary information
import shares


## New class called EncoderFSM
class ControllerFSM:
    '''
    @brief      A finite state machine to control the closed loop controller
    @details    This class implements a finite state machine to control and adjust the effort of the motor.
                
    '''
    
## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
## Constant defining State 1
    S1_WAITING     = 1    
    
## Constant defining State 2
    S2_CALCULATING     = 2    
    
      
    def __init__(self, MotorDriver, ClosedLoop, encoder, interval):
        '''
        @brief          Creates controller objects.
        
        '''
        
## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
## Intializes the encoder.
        
        self.encoder=encoder

## Intializes the Closed Loop class.
        self.ClosedLoop= ClosedLoop
 
## Intializes the Motor Driver.
        self.MotorDriver= MotorDriver

## Counter that describes the number of times the task has run
        self.runs = 0
        
##  The amount of time in milliseconds between runs of the task
        self.interval= int(interval*1e3)
        
## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

## Initializes motor effort
        self.L=0
        

        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
## Variable representing current time
        self.curr_time = utime.ticks_ms() #sets current time
        if (utime.ticks_diff(self.curr_time, self.next_time)>=0): #condition for state
            if(self.state == self.S0_INIT): 
                self.transitionTo(self.S1_WAITING) #begins transition
            #0 to 1 here
            
            elif(self.state == self.S1_WAITING): #if at state 1 do following 
                if shares.Kp!=None: #if theres a value
                    self.ClosedLoop.set_Kp(shares.Kp) #set that value
                    self.transitionTo(self.S2_CALCULATING) #transition
                    
            elif(self.state == self.S2_CALCULATING): #if state 2 then do following
                       self.encoder.update()#update position
                       self.MotorDriver.enable() #enable motor 
                       self.ClosedLoop.update() #update in Closed loop class
                       self.ClosedLoop.omegaref=shares.omegaref #set omega ref
                       #print (shares.omegaref)
                       self.L=self.ClosedLoop.L #set L
                       self.MotorDriver.set_duty(self.L) #set duty to L value
                       self.ClosedLoop.omegaact = (60000*self.encoder.updateddelta)/(self.interval*4000) #conversion to RPM
                       shares.omegaact= self.ClosedLoop.omegaact #set shares omega actual val
                       shares.times=self.interval*self.runs #set shares times
                       if (self.runs>=250): #if 5 seconds have passed do following
                               self.runs=0 #reset counter
                               self.MotorDriver.disable() #disable motor
                               shares.Kp=0 #reset Kp
                               self.transitionTo(self.S1_WAITING)#transition
                       self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval) 
            
                       
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState 

 