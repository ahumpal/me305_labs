#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file CLfrontend.py

Created on Fri Nov 20 19:01:52 2020


This file serves as a user interface, which is paired with the CL backend and Closed Loop Task.
It prompts the user for a Kp value and then runs the motor for 15 seconds. It then plots the step responses and calculates the performance metric.

Please that a Kp value above 0.223 causes overshoot. Please also note that I was only able to complete one run at time. If you want to use a another Kp value you have to rerun the frontend and main.


Created on Thu Oct 22 13:01:03 2020

@author Ashley Humpal
"""

import serial
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import time
from array import array 
## Variable representing serial connection
ser = serial.Serial(port='/dev/tty.usbmodem14103',baudrate=115273,timeout=1)
## Formatting list
Lis=[]
## List representing the actual omega values
omegaact1=[]
## List representing the actual position values
posi1=[]
## List representing the actual time values
times1=[]
# List Kp is appended to
la=[]
## Array representing the reference time values
timez = array('f',[])
## Array representing the reference velocity values
velocity = array('f',[])
## Array representing the reference position values
position = array('f',[])

## Function to write inputs
def sendChar():
## Variable representing user input
    Kp = float(input("Enter numerical Kp value:")) #Prompts user for input 
    la.append(Kp) #appends users input into list la 
    ser.write('{:f}\r\n'.format(la[0]).encode('ascii')) #writes list 




## Function to open CSV file    
def scv() :
    ref = open('reference.csv');
    while True:
        line = ref.readline()        # Read a line of data. 
        
        # If the line is empty, there are no more rows so exit the loop
        if line == '':
            break
        # If the line is not empty, strip special characters, split on commas, and
        # then append each value to its list.
        else:
            (t,v,x) = line.strip().split(',');
            timez.append(float(t))
            velocity.append(float(v))
            position.append(float(x))
    ref.close()
    
    
    
## Function to read outputs from other files
def receiveChar():
    time.sleep(20) #delay needed to give motor time to produce values before reading 
## Variable representing the decode line
    myval = ser.readline().decode('ascii') #decode information from the backend
    if myval !=0:  #if any key is read
## Intermediate variable stripping 
        line_list = myval.strip('[]\r\n')
## Intermediate variable splitting
        y= line_list.split('];[')	# strip any special characters and then split the string
        times=(y[0].split(',')) #time values moved to line_list position 0 and split 
        omegaact=(y[1].split(',')) #omega actual values moved to line_list position 1 and split 
        posi=(y[2].split(',')) #position actual values moved to line_list position 2 and split 
        for n in range (len(omegaact)): #for the length of the omega actual list do the following:
            omegaact1.append(float(omegaact[n])) #append values into new list called omegaact1
            times1.append(float(times[n])/1000) #append values into new list called times1
            posi1.append(float(posi[n])) #append values into new list called posi1
## J for Performance Metric
    summ=0
    for n in range (len(omegaact1)): #for every omenga actual value
        summ += (1/len(omegaact1))*(((velocity[n])-(omegaact1[n]))**2 +((position[n])-(posi1[n]))**2) #do this formula
    #print(summ)
    
    plt.subplot(2,1,1) #Subplot 1
    plt.plot(timez,velocity) #plot ref velocity
    plt.plot(times1, omegaact1) # plot actual velocity
    plt.legend(['Omega Desired','Omega Actual']) #legend
    plt.xlabel('Time [s]') #x axis label
    plt.ylabel('Velocity [RPM]') #y axis label
    plt.title('Step Response Kp=.22, J={:}'.format(summ)) #title
    
    
    plt.subplot(2,1,2) #Subplot 2
    plt.plot(timez, position)#plot ref position
    plt.plot(times1, posi1)# plot actual position
    plt.legend(['Position Desired','Position Actual']) #legend
    plt.xlabel('Time [s]') #x axis label
    plt.ylabel('Position [Degrees]') #y axis label

 

if __name__ == '__main__': 
        sendChar()
        scv()
        receiveChar()
