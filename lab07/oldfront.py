#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file CLfrontend.py

Created on Fri Nov 20 19:01:52 2020


This file serves as a user interface, which is paired with the CL backend and Closed Loop Task.
It prompts the user for a Kp value and then runs the motor for 5 seconds. It then plots the step response. 

Please that a Kp value above 0.3 causes overshoot. Please also note that I was only able to complete one run at time. If you want to use a another Kp value you have to rerun the frontend and main.


Created on Thu Oct 22 13:01:03 2020

@author Ashley Humpal
"""


import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import time
from array import array 
## Variable representing serial connection


## Formatting list
Lis=[]


## List representing the actual omega values
omegaact1=[]
posi1=[]

## Empty list representing time
times1=[]
# List Kp is appended to
la=[]

time = array('f',[])
velocity = array('f',[])
position = array('f',[])


def scv() :
    ref = open('ref50.csv');
    while True:
        # Read a line of data. It should be in the format 't,v,x\n' but when the
        # file runs out of lines the lines will return as empty strings, i.e. ''
        line = ref.readline()
        
        # If the line is empty, there are no more rows so exit the loop
        if line == '':
            break
        
        # If the line is not empty, strip special characters, split on commas, and
        # then append each value to its list.
        else:
            (t,v,x) = line.strip().split(',');
            time.append(float(t))
            velocity.append(float(v))
            position.append(float(x))
    ref.close()
    
    
    
#Function to read outputs from other files
def receiveChar():
    #time.sleep(4.96) #delay needed to give motor time to produce values before reading 
    #time.sleep(24.8)
    
 
   
    #step=[900]*250 #creates step array
    #plt.plot(times1, step) #plot these arrays
    plt.plot(time,velocity)
    
  
    
    plt.plot(time, position)
    
    
    plt.xlabel('Time [s]') #x axis label
    plt.ylabel('Velocity [RPM]') #y axis label
    plt.legend(['Omega Actual','Omega Desired']) #legend
    plt.title('Step Response for 900 RPM') #title
    
if __name__ == '__main__': 
       
        scv()
        receiveChar()