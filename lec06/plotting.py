#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 10:18:20 2020

@author: ashleyhumpal
"""
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
#numpy.arange

def plot(xnot):
    xnot=1
    xdotnot=0
    deltat=.1
    k=2
    alpha=5
    
    t=[0]
    x=[xnot]
    xdot=[xdotnot]
    xdoubledot=[-k*xnot-alpha*xnot*xnot*xnot]
    
    t= np.arange(0,10,deltat )
    
    
    for i in range(1,100):
       
        #t.append(t[i-1]+deltat)
        x.append(x[i-1] + xdot[i-1]*deltat + .5*xdoubledot[i-1]*deltat*deltat)
        xdoubledot.append(-k*x[i]-alpha*x[i]*x[i]*x[i])
        xdot.append(xdot[i-1]+.5*(xdoubledot[i-1] +xdoubledot[i])*deltat)
        
    #return(t,x)
        
(#t,x)=plot(.1)        
    matplotlib.pyplot.plot(t,x)

for index in range(1,10):
    plot(index/10)