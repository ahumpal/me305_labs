#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file Bluetooth_Module_Driver.py

This file serves as class file for the Bluetooth driver class in
Python. It will implement functions and commands using the Bluetooth device.

This script has code for the bluetooth class that utilizes the LED. 
Link to source code can be found here: 
    
https://bitbucket.org/ahumpal/me305_labs/src/master/lab05/Bluetooth_Module_Driver.py
Created on Thu Nov  5 14:23:48 2020

@author: Ashley Humpal
"""

import utime 
import pyb
from pyb import UART





## New class called Bluetooth
class Bluetooth:
    '''
    @brief      A finite state machine to control the bluetooth device
    @details    This class implements the LED pin A5
    '''
   
    def __init__(self,vall):
        self.vall=vall
        '''
        @brief          Creates a Bluetooth object.
        
        '''
 

        #self.period=0xFFFF
        
## Sets UART connection
        
        self.myuart=UART(3,9600)

## Sets pin connection
        
        self.pinA5 =pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        
        
    def write(self,inv): 
        '''
        @brief      Writes the user input so that it can be sent
        '''
        #inv = input('Give me frequency value')
        self.myuart.write(inv) #sends it to the board
        
    def read(self):
        '''
        @brief      Reads any user input
        '''
        
        myval = self.myuart.readline() 
        return myval
            #myval = self.myuart.readline().decode('ascii')
    
    def LEDon(self):
        '''
        @brief      Turns on the LED
        '''        
        self.pinA5.high()
    
    def LEDoff(self):
        '''
        @brief      Turns off the LED
        '''        
        self.pinA5.low()
    
        #self.tim.callback(None)
    def getcheck(self):
     
        '''
        @brief      Checks if key has been pressed
        '''
        
        value=self.myuart.any()
        return value
        

    
 