#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 14:36:56 2020

@author: ashleyhumpal
"""

# cooperativve task - similar to ui 
# waiting for number nd tehn get it and update frequency of led freq pattern
# no pwm cuz only 1-10 hz 
# no blocking 

# import serial
# import matplotlib
# import numpy as np
# import matplotlib.pyplot as plt
# import keyboard 

import utime 
import pyb
from pyb import UART
from Bluetooth_Module_Driver import Bluetooth


## New class called collecting
class Frequency: 
    '''
    @brief      A finite state machine to control the data collection of the encoder.
    @details    This class implements a finite state machine to control the
                sampling of time and position data points from the encoder.
    '''
    
## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
## Constant defining State 1
    S1_WAITING     = 1    
    
## Constant defining State 2
    S2_ON   = 2 
    
    S3_OFF   = 3
    
    


  
    
      
    def __init__(self, Bluetooth): #,interval
        '''
        @brief          Creates an encoder object.
        
        '''
        
## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        

        

## Counter that describes the number of times the task has run
        self.runs = 0
        
##  The amount of time in milliseconds between runs of the task
        self.interval= 1
        
        #self.interval= int(interval*1e3)
        
## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        self.Bluetooth= Bluetooth

        
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
## Variable representing current time
        self.curr_time = utime.ticks_ms() #sets current time
        if (utime.ticks_diff(self.curr_time, self.next_time)>=0): #condition for state
            if(self.state == self.S0_INIT): 
                self.transitionTo(self.S1_WAITING) #begins transition
            #0 to 1 here
         
            
         
            
            elif(self.state == self.S1_WAITING): #if at state 1 do following 
                self.Bluetooth.read()
                freq=self.Bluetooth.read()
                self.interval= int(.5*(1e3/freq))
                
                if self.Bluetooth.getcheck() > 0 and self.Bluetooth.getcheck()<=10: #is this correct tho with ascii
                        self.transitionTo(self.S2_ON )   #any other key then transition back to state 1
                else:
                    self.transitionTo(self.S1_WAITING) 
                       
                
             
             #make 2 , 1 counter based of inter*selfruns to represent real time,and one based off freeq cuz freq is onlu variabkes and not real value
            elif(self.state == self.S2_ON): #if state 2 then do following
                 
                 if (self.getcheck()==0):
                     self.Bluetooth.LEDon
                     self.transitionTo(self.S3_OFF) 
                 else:
                     (self.transitionTo(self.S1_WAITING))
                     
            elif(self.state == self.S3_OFF):
                if (self.getcheck()==0):
                     self.Bluetooth.LEDoff
                     self.transitionTo(self.S2_ON) 
                else:
                     (self.transitionTo(self.S1_WAITING))
                
                
            self.runs += 1
                       
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState    
                     
                     
            
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
    #                  if (self.interval*self.runs<runtime):
    #                      self.Bluetooth.LEDon
    #                  #if self.Bluetooth.read()!=0:
                         
    #                  elif (self.interval*self.runs>=runtime):
    #                     # if  self.interval>= 1/(.5*freq):
    #                      self.Bluetooth.LEDoff
                        
    #                  else:       
    #                      self.transitionTo(self.S2_BLINKING )
    #              elif(self.getcheck()!=0):
    #                  self.transitionTo(self.S1_WAITING )
    #              else :
    #                  self.transitionTo(self. S1_WAITING)

    #              self.runs += 1
                     
    #                 #  freq=self.Bluetooth.read()
    #                 #  runtime= 1/(.5*freq)
    #                 # # self.interval= 1/freq #should this be 1/.5freq
                    
                     
    #                 #  if (self.interval*self.runs<runtime):
    #                 #      self.Bluetooth.LEDon
    #                 #  #if self.Bluetooth.read()!=0:
    #                 #      self.transitionTo(self.S2_BLINKING )
    #                 #  elif (self.interval*self.runs>=runtime):
    #                 #     # if  self.interval>= 1/(.5*freq):
    #                 #      self.Bluetooth.LEDoff
    #                 #  else:       
    #                 #      self.transitionTo(self.S2_BLINKING )
                 
    
                
                
            
                       
    # def transitionTo(self, newState):
    #     '''
    #     @brief      Updates the variable defining the next state to run
    #     '''
    #     self.state = newState 
         
 