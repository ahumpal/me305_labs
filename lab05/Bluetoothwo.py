#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file Bluetoothwo.py

This file serves as an implementation of a finite-state-machine using
micropython. It will implement code to have an LED blink on and off at a specified frequency between 1 and 10 Hz.

Link to source code can be found here: 
    
https://bitbucket.org/ahumpal/me305_labs/src/master/lab05/Bluetoothwo.py

Created on Thu Nov  5 14:36:56 2020

@author: Ashley Humpal
"""

import utime 
import pyb
from pyb import UART
#Note BMD: denotes the alternative code if paired with Bluetooth Module Driver 


## New class called Frequency 
class Frequency: 
    '''
    @brief      A finite state machine to control the blinking frequency of the LED.
    @details    This class implements a finite state machine to control the
               rate at which the LED blinks.
    '''
    
## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
## Constant defining State 1
    S1_WAITING     = 1    
    
## Constant defining State 2
    S2_ON   = 2 
## Constant defining State 3
    S3_OFF   = 3
    
    

    def __init__(self): 
        '''
        @brief          Creates a frequency object.
        
        '''
        
## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        

        

## Counter that describes the number of times the task has run
        self.runs = 0
        
##  The amount of time in milliseconds between runs of the task
        self.interval= int(1e3)

## Initial frequency value
        self.freq=0
        
        
## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
## Setting up UART
        self.myuart=UART(3,9600) #not needed if Bluetooth Module Driver Working

## Setting up pin A5
        self.pinA5 =pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)#not needed if Bluetooth Module Driver Working
        
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
## Variable representing current time
        self.curr_time = utime.ticks_ms() #sets current time
        if (utime.ticks_diff(self.curr_time, self.next_time)>=0): #condition for state
            if(self.state == self.S0_INIT): 
                self.transitionTo(self.S1_WAITING) #begins transition
            #0 to 1 here
         
        
            
            elif(self.state == self.S1_WAITING): #if at state 1 do following 
                if self.myuart.any() > 0 and self.myuart.any()<=10: #if freq between 0 and 10 then
                #BMD:if self.Bluetooth.getcheck() > 0 and self.Bluetooth.getcheck()<=10:
                        self.freq=int(self.myuart.readline()) #read value
                        #BMD:self.freq=int(self.Bluetooth.read())
                        self.interval= int(0.5*(1e3/self.freq)) #interval equals half of the frequency reciprocal
                        self.transitionTo(self.S2_ON )   #transition state 2
                else: #anything else pressed
                    self.transitionTo(self.S1_WAITING)  #transition back to state 1
                self.myuart.write('LED frequency is' + str(self.freq)+ 'Hz') #write back to Phone app
                #BMD:self.Bluetooth.write('LED frequency is' + str(self.freq)+ 'Hz')
                
             
            
            elif(self.state == self.S2_ON): #if state 2 then do following
                 if (self.myuart.any()!=0): #if key pressed
                  #BMD:if (self.Bluetooth.getcheck()!=0 )
                     (self.transitionTo(self.S1_WAITING)) #transition state 1   
                 else: #otherwise
                     self.pinA5.high() # turn on pin
                     #BMD:self.Bluetooth.LEDon()
                     self.transitionTo(self.S3_OFF) #transition state3
                     
            elif(self.state == self.S3_OFF): #if state 3
                if (self.myuart.any()!=0): #if key pressed
                #BMD:if (self.Bluetooth.getcheck()!=0 )
                    self.transitionTo(self.S1_WAITING) #transition state 1
                else: #otherwise
                    self.pinA5.low() #turn off pin
                    #BMD:self.Bluetooth.LEDoff()
                    self.transitionTo(self.S2_ON) #transition state 2
                     
 ## Counter for number of runs               
            self.runs += 1
  
## Specifies time for next run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
                       
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState    
                     
                     
            
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
   