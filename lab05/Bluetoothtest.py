#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov  8 11:56:33 2020

@author: ashleyhumpal
"""
import pyb
from pyb import UART 

uart= UART(3,9600)
pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)

while True:
    if uart.any() != 0:
        val = int(uart.readline())
        if val == 0:
            print(val,' turns it OFF')
            pinA5.low()
        elif val == 1:
            print(val,' turns it ON')
            pinA5.high()
