#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file UI.py
This file serves as an User Interface through the use of a Finite State System.
Certain keys being pressed will trigger different functions in the encoder.

Link to UI source code can be found here:

https://bitbucket.org/ahumpal/me305_labs/src/master/lab03/UI.py


Created on Tues Oct 13, 2020

@author Ashley Humpal
"""

from random import choice
import utime
from Encoderclass import encoder
from pyb import UART
myuart= UART(2)


## New class called UI
class UI:
    '''
    @brief      A finite state machine to control the User Interface.
    @details    This class implements a finite state machine to indicate which keys 
                control the different encoder operations.
    '''
    
## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
## Constant defining State 1
    S1_PROCESS_USER_INPUT     = 1    
    
      
    def __init__(self, interval):
    
        '''
        @brief          Creates a User Interface object.
        
        '''
        
## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
                        
## Counter that describes the number of times the task has run
        self.runs = 0
        
##  The amount of time in seconds between runs of the task
        #self.interval = interval
        self.interval= int(interval*1e3)
        
## The timestamp for the first iteration
        #self.start_time = time.time()
        self.start_time = utime.ticks_us()
        
## The "timestamp" for when the task should run next
        #self.next_time = self.start_time + self.interval
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
## The UI object for the current time is created.
        #self.curr_time = time.time() #initializes variable for time
        self.curr_time = utime.ticks_us()
        #if self.curr_time > self.next_time: #if the current time exceeds time of when next task runs do following
        if (utime.ticks_diff(self.curr_time, self.next_time)>=0):
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_PROCESS_USER_INPUT) #begins transition
                #print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
                #print('User input required. Press "z" to zero the encoder, "p" to print encoder position, "d" print delta value.')
            #0 to 1 here
            
            
            elif(self.state == self.S1_PROCESS_USER_INPUT): #if reads character input do following
                # Run State 1 Code
                if(myuart.any()):
                    if myuart.readchar().decode(122): # if z ascii key then do following (decimal)
                        encoder.setposition(0)
                        print(encoder.setposition)
                        print('User input required. Press "z" to zero the encoder, "p" to print encoder position, "d" print delta value.')
                        
                    elif myuart.readchar().decode(112):#if p  ascii key then do following
                        encoder.getposition() #use getposition from encoder class
                        print(encoder.getposition)
                        print('User input required. Press "z" to zero the encoder, "p" to print encoder position, "d" print delta value.')
                        
                        
                    elif myuart.readchar().decode(100):#if d  ascii key then do following
                        encoder.getdelta() #use getdelta from encoder class
                        print(encoder.getdelta)
                        print('User input required. Press "z" to zero the encoder, "p" to print encoder position, "d" print delta value.')
            
            else:
                # Invalid state code (error handling)
                    print('User input required. Press "z" to zero the encoder, "p" to print encoder position, "d" print delta value.')
                    
            
            self.runs += 1
            
            # Specifying the next time the task will run
            #self.next_time = self.next_time + self.interval
            self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    def transitionTo(self, newState): #defines transitionTo state
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
    
        
        


    
    
    
    
    
    
    
    
    
    
    
    
    