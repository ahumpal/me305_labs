#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file Encoderclass.py

This file serves as class file for the encoder driver class in
Python. It will implement functions and commands using the encoder.

This script has code for the encoder class that utilizes the encoder's timer. 
Link to source code can be found here: 
    
https://bitbucket.org/ahumpal/me305_labs/src/master/lab03/Encoderclass.py

Created on Fri Oct 13, 2020

@author Ashley Humpal
"""

#LEFT IN MICROPYTHON INSTEAD OF PYTHON BECAUSE ALL CODE DEPENDS ON MICRO 
from random import choice

import pyb

## New class called encoder
class encoder:
    '''
    @brief      A finite state machine to control the 
    @details    This class implements 
    '''


      
    def __init__(self):
   
        '''
        @brief          Creates a object.
        @param pinA6  An object from class encoder representing pinA6.
        @param pinA7 An object from class encoder representing pinA7.
        @param tim An object rom class encoder representing the encoder counter.
        '''
        
        
## The encoder object used for the encoder timer is created
        self.tim.counter =0
        

## The encoder object for the final position is created
        self.position= 0
        
## The encoder object for the previus position is created
        self.previousposition=0
        
## The encoder object for the period length is created
        self.period=0xFFFF
        
        #self.delta=0

## The encoder object for the updated delta is created
        self.updateddelta=0
        
        tim= pyb.Timer(3) #Creating variable for the encoder timer
## The encoder object for the prescaler is created.
        tim.init(prescaler=3, period=self.period) # initializes timer parameters
## The encoder object for the pin A6 is created.
        tim.channel(1, pin=pyb.Pin.cpu.A6, mode=pyb.Timer.ENC_AB) #sets up channel 1
## The encoder object for the encoder mode is created.
## The encoder object for A7 is created.
        tim.channel(2, pin=pyb.Pin.cpu.A7, mode=pyb.Timer.ENC_AB) #sets up channel 2
   
        
    def update( self): 
        '''
        @brief      Updates the position of the encoder by adding good delta values
        '''
## The encoder object for the current position is created
        self.currentposition= self.tim.counter #current position
        delta=self.currentposition-self.previousposition
        self.previousposition=self.currentposition
## The encoder object for the delta value between runs is created.
        self.delta=delta
        deltamag=abs(delta)
## The encoder object for the absolute value of delta is created
        self.deltamag = deltamag
        #print(self.tim.period)
       
        if self.deltamag> .5*(self.period): 
            if self.delta<0:
               update= self.delta +self.period
               self.updateddelta=update
               
            elif self.delta>0:
                update = self.delta - self.period
                self.updateddelta=update
        else:
            pass
        self.position= self.position+ self.updateddelta
        
    def getposition(self):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        return self.tim.counter()
    
    def setposition(self,datum):
        '''
        @brief      Updates the variable defining the next state to run
        '''        
        return self.tim.counter(datum)
    
        #self.tim.callback(None)
    def getdelta(self,delta, tim,i):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        
        return self.updateddelta
        
    
    
    
    #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file Encoderclass.py

This file serves as class file for the encoder driver class in
Python. It will implement functions and commands using the encoder.

This script has code for the encoder class that utilizes the encoder's timer. 
Link to source code can be found here: 
    
https://bitbucket.org/ahumpal/me305_labs/src/master/lab03/Encoderclass.py

Created on Fri Oct 13, 2020

@author Ashley Humpal
"""

#LEFT IN MICROPYTHON INSTEAD OF PYTHON BECAUSE ALL CODE DEPENDS ON MICRO 
from random import choice

import pyb

## New class called encoder
class encoder:
    '''
    @brief      A finite state machine to control the 
    @details    This class implements 
    '''


      
    def __init__(self, pinA6, pinA7, tim):
   
        '''
        @brief          Creates a object.
        @param pinA6  An object from class encoder representing pinA6.
        @param pinA7 An object from class encoder representing pinA7.
        @param tim An object rom class encoder representing the encoder counter.
        '''
        


## The encoder object used for referencing the connected pin A6 is created
        self.pinA6 = pinA6
        
## The encoder object used for referencing the connected pin A7 is created
        self.pinA7 = pinA7
        
## The encoder object used for the encoder timer is created
        self.tim = tim
        

## The encoder object for the final position is created
        self.position= 0
        
## The encoder object for the previus position is created
        self.previousposition=0
        
## The encoder object for the period length is created
        self.period=0xFFFF
        
        #self.delta=0

## The encoder object for the updated delta is created
        self.updateddelta=0
        
        tim= pyb.Timer(3) #Creating variable for the encoder timer
## The encoder object for the prescaler is created.
        tim.init(prescaler=3, period=self.period) # initializes timer parameters
## The encoder object for the pin A6 is created.
        tim.channel(1, pin=pyb.Pin.cpu.A6, mode=pyb.Timer.ENC_AB) #sets up channel 1
## The encoder object for the encoder mode is created.
## The encoder object for A7 is created.
        tim.channel(2, pin=pyb.Pin.cpu.A7, mode=pyb.Timer.ENC_AB) #sets up channel 2
   
        
    def update( self): 
        '''
        @brief      Updates the position of the encoder by adding good delta values
        '''
## The encoder object for the current position is created
        self.currentposition= self.tim.counter #current position
        delta=self.currentposition-self.previousposition
        self.previousposition=self.currentposition
## The encoder object for the delta value between runs is created.
        self.delta=delta
        deltamag=abs(delta)
## The encoder object for the absolute value of delta is created
        self.deltamag = deltamag
        #print(self.tim.period)
       
        if self.deltamag> .5*(self.period): 
            if self.delta<0:
               update= self.delta +self.period
               self.updateddelta=update
               
            elif self.delta>0:
                update = self.delta - self.period
                self.updateddelta=update
        else:
            pass
        self.position= self.position+ self.updateddelta
        
    def getposition(self):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        return self.tim.counter()
    
    def setposition(self,datum):
        '''
        @brief      Updates the variable defining the next state to run
        '''        
        return self.tim.counter(datum)
    
        #self.tim.callback(None)
    def getdelta(self,delta, tim,i):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        
        return self.updateddelta
        
        
        




        
        



